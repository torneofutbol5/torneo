package com.steven.dao;

import com.steven.bd.Conexion;
import com.steven.interfaces.IPersonaDAO;
import com.steven.model.Persona;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class ImplPersonaDAO implements IPersonaDAO {

    private Conexion conexion;
    private Connection connection;

    @Override
    public void crearPersona(Persona persona) throws SQLException {
        connection = conexion.establecerConexion();
        String insertPersona = "INSERT INTO torneo_futbol_schema.persona (id, nombre, apellido) VALUES (?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(insertPersona);
        preparedStatement.setLong(1, persona.getId());
        preparedStatement.setString(2, persona.getNombre());
        preparedStatement.setString(3, persona.getApellido());
        preparedStatement.executeUpdate();
        connection.close();
    }
}
