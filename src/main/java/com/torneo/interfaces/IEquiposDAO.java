package com.steven.interfaces;

import com.steven.model.Equipo;

public interface IEquiposDAO {

    public Equipo mostrarEquipoId(Long id);
    public boolean mostrarEquipoNombre(String nombreEquipo);

}
