package com.steven.interfaces;

import com.steven.model.Persona;

import java.sql.SQLException;

public interface IPersonaDAO {

    void crearPersona(Persona persona) throws SQLException;

}
