package com.steven.model;

public class Tecnico extends Persona {

    private Equipo equipoDirigir;

    public Equipo getEquipoDirigir() {
        return equipoDirigir;
    }

    public void setEquipoDirigir(Equipo equipoDirigir) {
        this.equipoDirigir = equipoDirigir;
    }
}
