package com.steven.model;

public class Equipo {

    private static Long ID;
    private String nombreEquipo;

    public Equipo(){
        ID++;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }
}
