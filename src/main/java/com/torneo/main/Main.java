package com.steven.main;

import com.steven.dao.ImplPersonaDAO;
import com.steven.interfaces.IPersonaDAO;
import com.steven.model.Persona;

import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("1.Crear Personas\n2.Crear jugador\n3.Crear Equipo");

        int select = sc.nextInt();

        switch (select){
            case 1:
                System.out.println("Crear Persona");
                Scanner nombrePersona = new Scanner(System.in);
                Scanner apellidoPersona = new Scanner(System.in);
                Scanner idPersona = new Scanner(System.in);
                IPersonaDAO personaDAO = new ImplPersonaDAO();
                Persona persona = new Persona();
                persona.setId(idPersona.nextLong());
                persona.setNombre(nombrePersona.nextLine());
                persona.setApellido(apellidoPersona.nextLine());

                try{
                    personaDAO.crearPersona(persona);
                }catch(SQLException e){
                    e.printStackTrace();
                }

                break;

            case 2:
                break;

            case 3:
                break;
        }
    }

}
