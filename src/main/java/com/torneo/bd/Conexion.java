package com.steven.bd;

import jdk.dynalink.linker.ConversionComparator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    private static Connection con;
    private static String DRIVER = "org.postgresql.Driver";

    public static Connection establecerConexion(){
        try {
            Class.forName(DRIVER);
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/torneo_futbol", "postgres", "123456789");
            System.out.println("Se establecio la conexión correctamente");
        }catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }

        return con;
    }

    public static void cerrarConexion() throws SQLException{
        if(con != null)
            con.close();
    }
}
